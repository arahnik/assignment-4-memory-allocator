#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header* block) { return block->capacity.bytes >= query; }
static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void* restrict addr, block_size block_sz, void* restrict next) {
    *((struct block_header *)addr) = (struct block_header){
        .next = next,
        .capacity = capacity_from_size(block_sz),
        .is_free = true
    };
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region* r);


static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap((void *)addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(const void *addr, size_t query) {
    const size_t block_header_size = offsetof(struct block_header, contents);
    const size_t region_size = region_actual_size(query + block_header_size);

    void *actual_addr = map_pages(addr, region_size, MAP_FIXED);
    if (actual_addr == MAP_FAILED) {
        actual_addr = map_pages(addr, region_size, 0);
        if (actual_addr == MAP_FAILED) {
            return REGION_INVALID;
        }
    }

    struct region region = {
        .addr = actual_addr,
        .size = region_size,
        .extends = (actual_addr == addr)
    };

    block_init(region.addr, (block_size){region_size}, NULL);

    return region;
}

static void* block_after(struct block_header const* block);

void* heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;
    return region.addr;
}

static bool blocks_continuous(
    struct block_header const* fst,
    struct block_header const* snd) {
    return (void *)snd == block_after(fst);
}

/*  освободить всю память, выделенную под кучу */
void heap_term() {
    void* start = HEAP_START;
    struct block_header* current = start;
    size_t size = 0;

    while (current) {
        size += size_from_capacity(current->capacity).bytes;
        struct block_header* buff = current->next;
        if (!blocks_continuous(current, buff)) {
            munmap(start, size);
            start = buff;
            size = 0;
        }
        current = buff;
    }
}



#define BLOCK_MIN_CAPACITY 24

static bool block_splittable(struct block_header* restrict block, size_t query) {
    return block->is_free && query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.
           bytes;
}

static bool split_if_too_big(struct block_header* block, size_t query) {
    if (!block_splittable(block, query)) return false;
    const size_t capacity = block->capacity.bytes;
    block->capacity.bytes = query;
    void* child = block_after(block);
    block_init(child, (block_size){capacity - query}, block->next);
    block->next = child;
    return true;
}


static void* block_after(struct block_header const* block) {
    return (void *)(block->contents + block->capacity.bytes);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header* block) {
    if (block && block->next && mergeable(block, block->next)) {
        block_size size = {size_from_capacity(block->capacity).bytes + size_from_capacity(block->next->capacity).bytes};
        block_init(block, size, block->next->next);
        return true;
    }
    return false;
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum { BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED } type;

    struct block_header* block;
};


static struct block_search_result find_good_or_last(struct block_header* restrict block, size_t sz) {
    if (!block) return (struct block_search_result){.type = BSR_CORRUPTED};
    struct block_header* curr = block;
    struct block_header* last = NULL;
    while (curr) {
        while (try_merge_with_next(curr)) {}
        if (block_is_big_enough(sz, curr) && curr->is_free)
            return (struct block_search_result){
                .block = curr, .type = BSR_FOUND_GOOD_BLOCK
            };
        last = curr;
        curr = curr->next;
    }
    return (struct block_search_result){.block = last, .type = BSR_REACHED_END_NOT_FOUND};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header* block) {
    struct block_search_result ret = find_good_or_last(block, query);
    if (ret.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(ret.block, query);
        ret.block->is_free = false;
    }
    return ret;
}


static struct block_header* grow_heap(struct block_header* restrict last, size_t query) {
    struct region reg = alloc_region(block_after(last), query);
    if (region_is_invalid(&reg)) return NULL;
    last->next = reg.addr;
    if (reg.extends && try_merge_with_next(last)) return last;
    return last->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc(size_t query, struct block_header* heap_start) {
    struct block_search_result ret = try_memalloc_existing(size_max(query, BLOCK_MIN_CAPACITY), heap_start);
    if (!ret.type) return ret.block;
    if (ret.type == 1) {
        ret = try_memalloc_existing(size_max(query, BLOCK_MIN_CAPACITY), grow_heap(ret.block, size_max(query, BLOCK_MIN_CAPACITY)));
        if (!ret.type) return ret.block;
    }
    return NULL;
}


void* _malloc(size_t query) {
    if (!query) return NULL;
    struct block_header* const addr = memalloc(query, (struct block_header *)HEAP_START);
    if (addr) return addr->contents;
    return NULL;
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header *)((uint8_t *)contents - offsetof(struct block_header, contents));
}

void _free(void* mem) {
    if (!mem) return;
    struct block_header* header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header)) {};
}
