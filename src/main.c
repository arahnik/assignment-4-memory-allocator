#define _GNU_SOURCE

#include <assert.h>

#include "mem.h"
#include <inttypes.h>

void debug(const char*, ...);

void test(void (*test_body)(void*), const char* prelog) {
    debug(prelog);
    void* heap = heap_init(0);
    (*test_body)(heap);
    heap_term();
    debug("!!! - SUCCESS - !!!");
}

void allocation_test(void* heap) {
    void* block = _malloc(100 * sizeof(int64_t));
    debug_heap(stderr, heap);
    _free(block);
    debug_heap(stderr, heap);
}

void free_one_test(void* heap) {
    _malloc(500);
    void* block = _malloc(666);
    debug_heap(stderr, heap);
    _free(block);
    debug_heap(stderr, heap);
}

void free_two_test(void* heap) {
    _malloc(1);
    void* first = _malloc(2);
    _malloc(3);
    _malloc(4);
    void* second = _malloc(123);
    debug_heap(stderr, heap);
    _free(first);
    _free(second);
    debug_heap(stderr, heap);
}

void grow_reg_test(void* heap) {
    debug_heap(stderr, heap);
    void* reverse_technic = _malloc(5 * 4096);
    debug_heap(stderr, heap);
    _free(reverse_technic);
    debug_heap(stderr, heap);
}

void new_reg_test(void* heap) {
    void* step1 = mmap(heap + 4096, 4096, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);
    void* step2 = _malloc(123);
    debug_heap(stderr, heap);
    void* step3 = _malloc(408);
    debug_heap(stderr, heap);
    _free(step2);
    debug_heap(stderr, heap);
    _free(step3);
    debug_heap(stderr, heap);
    _free(step1);
    debug_heap(stderr, heap);
}

int main() {
    test(allocation_test, "Обычное успешное выделение памяти");
    test(free_one_test, "Освобождение одного блока из нескольких выделенных");
    test(free_two_test, "Освобождение двух блоков из нескольких выделенных");
    test(grow_reg_test, "Расширение территории _/\\_");
    test(new_reg_test, "Выделение нового региона в новом месте");
    return 0;
}
